const Cart = require("../models/cart");
const Product = require("../models/products"); // Add this line to import the Product model


// Get User's Cart
module.exports.getUserCart = (req, res) => {
  const userId = req.user.id;

  Cart.findOne({ user: userId })
    .populate('items.product', 'name price') // Populate product details in the items array
    .then((cart) => {
      if (!cart) {
        return res.status(404).send({ message: 'Cart not found for the user' });
      }

      res.status(200).send({ cart });
    })
    .catch((err) => {
      console.error('Error in getting user cart:', err);
      res.status(500).send({ error: 'Failed to get user cart' });
    });
};


// Add to Cart
module.exports.addToCart =  (req, res) => {
  try {
    const userId = req.user.id;
    const { productId, quantity } = req.body;
    // const productId = req.body.productId;
    // const quantity = req.body.quantity;

    // Retrieve product details
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).send({ error: 'Product not found' });
    }

    // Calculate subtotal
    const subtotal = product.price * quantity;

    let userCart = await Cart.findOne({ userId });

    if (!userCart) {
      userCart = new Cart({
        userId,
        items: [],
        total: 0,
      });
    }


    // Add a new item or update existing item
    const existingItem = userCart.items.find(item => item.productId === productId);

    if (existingItem) {
      existingItem.quantity += quantity;
      existingItem.subtotal += subtotal;
    } else {
      userCart.items.push({
        productId,
        quantity,
        subtotal,
      });
    }

    // Recalculate the total in the cart
    userCart.total = userCart.items.reduce((acc, item) => acc + item.subtotal, 0);

     // Recalculate total in the cart
    // const total = updatedCart.items.reduce((acc, item) => acc + item.subtotal, 0);

    // Update or create the user's cart
    // const updatedCart = new Cart(
    //   { userId , items ,  total : 0 }
    // );

    // updatedCart.total = total;
     await updatedCart.save();

    return res.status(200).send({ message: 'Item added to cart successfully', cart: updatedCart });
  } catch (error) {
    console.error('Error in adding to cart:', error);
    res.status(500).send( error );
  }
};


// Change Product Quantities in Cart
// Change Product Quantities in Cart
module.exports.changeQuantity = async (req, res) => {
  try {
    const userId = req.user.id;
    const productId = req.body.productId;
    const newQuantity = req.body.quantity;

    // Find the user's cart
    const userCart = await Cart.findOne({ user: userId });

    if (!userCart) {
      return res.status(404).send({ error: 'Cart not found for the user' });
    }

    // Find the index of the item in the cart
    const itemIndex = userCart.items.findIndex((item) => item.product.equals(productId));

    if (itemIndex === -1) {
      return res.status(404).send({ error: 'Product not found in the cart' });
    }

    // Update the quantity and recalculate subtotal
    userCart.items[itemIndex].quantity = newQuantity;
    userCart.items[itemIndex].subtotal = newQuantity * userCart.items[itemIndex].product.price;

    // Recalculate total in the cart
    const total = userCart.items.reduce((acc, item) => acc + item.subtotal, 0);
    userCart.total = total;

    // Save the updated cart
    await userCart.save();

    res.status(200).send({ message: 'Quantity updated successfully', cart: userCart });
  } catch (error) {
    console.error('Error in changing quantity:', error);
    res.status(500).send({ error: `Failed to update quantity in cart. ${error.message}` });
  }
};



// Remove Product from Cart
module.exports.removeProductFromCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const productId = req.params.productId;

    // Find the user's cart
    const userCart = await Cart.findOne({ user: userId });

    if (!userCart) {
      return res.status(404).send({ error: 'Cart not found for the user' });
    }

    // Check if the product is in the cart
    const cartItem = userCart.items.find((item) => item.product.equals(productId));

    if (!cartItem) {
      return res.status(404).send({ error: 'Product not found in the cart' });
    }

    // Remove the product from the items array
    userCart.items.pull({ _id: cartItem._id });

    // Recalculate total in the cart
    const total = userCart.items.reduce((acc, item) => acc + item.subtotal, 0);
    userCart.total = total;

    // Save the updated cart
    await userCart.save();

    res.status(200).send({ message: 'Product removed from the cart successfully', cart: userCart });
  } catch (error) {
    console.error('Error in removing product from cart:', error);
    res.status(500).send({ error: 'Failed to remove product from cart' });
  }
};

// Clear All Cart Items
module.exports.clearCartItems = async (req, res) => {
  try {
    const userId = req.user.id;

    // Find and update the user's cart
    const updatedCart = await Cart.findOneAndUpdate(
      { user: userId },
      { $set: { items: [], total: 0 } },
      { new: true }
    );

    if (!updatedCart) {
      return res.status(404).send({ error: 'Cart not found for the user' });
    }

    res.status(200).send({ message: 'All cart items cleared successfully', cart: updatedCart });
  } catch (error) {
    console.error('Error in clearing cart items:', error);
    res.status(500).send({ error: 'Failed to clear cart items' });
  }
};


