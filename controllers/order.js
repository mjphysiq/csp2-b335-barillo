const Order = require("../models/order");

// Create Order for Authenticated User
module.exports.createOrder = async (req, res) => {
  try {
    const userId = req.user.id;

    // Retrieve order details from the request body
    // You may pass additional details such as shipping information, payment method, etc., as needed
    const { items, total, /* other order details */ } = req.body;

    const newOrder = new Order({
      user: userId,
      items,
      total,
      // Include other order details if needed
    });

    // Save the new order
    await newOrder.save();

    res.status(201).send({ message: 'Order created successfully', order: newOrder });
  } catch (error) {
    console.error('Error in creating order for authenticated user:', error);
    res.status(500).send({ error: 'Failed to create order' });
  }
};

// Retrieve All Orders for Authenticated User
module.exports.getAllOrders = async (req, res) => {
  try {
    const userId = req.user.id;

    // Retrieve all orders for the authenticated user
    const userOrders = await Order.find({ user: userId });

    res.status(200).send({ orders: userOrders });
  } catch (error) {
    console.error('Error in retrieving all orders for authenticated user:', error);
    res.status(500).send({ error: 'Failed to retrieve orders' });
  }
};

// Retrieve All Users' Orders (Admin Only)
module.exports.getAllUsersOrders = async (req, res) => {
  try {
    // Verify the JWT token and check if the user is an admin
    const token = req.headers.authorization;
    const decodedToken = jwt.verify(token, config.secret); // Make sure to replace with your actual secret

    if (!decodedToken.isAdmin) {
      return res.status(403).send({ error: 'Permission denied. Only admins can retrieve all users\' orders.' });
    }

    // Retrieve all orders for all users
    const allUsersOrders = await Order.find({});

    res.status(200).send({ orders: allUsersOrders });
  } catch (error) {
    console.error('Error in retrieving all users\' orders (admin):', error);
    res.status(500).send({ error: 'Failed to retrieve orders' });
  }
};